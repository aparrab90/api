# ora_settings.py
# ----------------------------------------
import base64

ORA_CONNECTION = 'ups-scan.ups.edu.ec:1521/ups.edu.ec'
ORA_USERNAME = 'aparrab'
ORA_PASSWORD = 'AApp_27082021'
SQLS = [
"""select * from expl.matgeneral j where j.periodo=64 and J.sede='GUAYAQUIL' AND J.mat_tipo_proceso='R'""",
    "SELECT * FROM expl.matgeneral WHERE periodo>59 and pagado= 'S'",
    "SELECT * FROM expl.matgeneral WHERE periodo>59 and pagado= 'N'",
    "select * from ledm where pel_Codigo>62",
    """
    SELECT
    deecampus.dee_descripcion                            campus,
    pre.pre_codificacion                                 carrera,
           MOD.mod_descripcion modalidad,
    cl.cllc_ruc                                    cedula,
        (select jo.jor_descripcion from INS.ins_jornada jo
    where jo.jor_codigo=o.jor_codigo)jornada,
    cl.cllc_nmb,
    c.ins_codigo,
    a.alu_codigo,
    a.alu_email      "CORREO INSTITUCIONAL",
    cl.cllc_email                                        "CORREO PERSONAL",
    cl.cllc_fono                                         "TELEFONO 1",
    cl.cllc_celular                                      "TELEFONO 2",
(select distinct nvl(ieh.INE_DESCRIPCION_H, ieh.ine_descripcion)  from 
(select alu_codigo, max(ine_codigo) INE_CODIGO 
from ins.ins_alu_ins_edu
where aud_eliminado = 'N' 
group by alu_codigo)aie,
 bsc.bsc_instituciones_educativas_homologadas ieh 
 where aie.INE_CODIGO = ieh.ine_codigo
 and  a.alu_codigo = aie.alu_codigo)colegio,
    decode(
    (
        SELECT
            LISTAGG(''
                    ||  p.mat_pagado
                    || '', '/') WITHIN GROUP(
                ORDER BY
                    p.mat_pagado)
           -- COUNT(*)
          FROM
            mat.mat_matricula p,
              org.org_periodo_estructura pee
        WHERE
                p.ins_codigo = c.ins_codigo
            AND p.mat_tipo_proceso = 'R'
            AND p.aud_eliminado = 'N'
            AND p.mat_anulado = 'N'
            AND  pee.pee_codigo  = p.pee_codigo
            and pee.pel_codigo=o.pel_codigo
    ),
           'S',
           'MATRICULA LEGALIZADA',
            'S/S',
           'MATRICULA LEGALIZADA',
           'N',
           'PREMATRICULADO',
           'SIN MATRICULAR')                          "PAGO MATRICULA?",



(SELECT DECODE(COUNT(*),1,'SI','NO') FROM INS.ins_alu_ins_edu P
WHERE P.ALU_CODIGO= c.alu_codigo
AND (P.AIE_VIGENTE= 'S' AND P.AIE_INSTITUCION_CONFIRMADO='S'))"CONFIRMACION DE INSTITUCION EDUCATIVA",

c.ins_fecha_inscripcion FECHA_INSCRIPCION,

    (
        SELECT

             LISTAGG('  '
                    ||    ( p.aud_fecha_adicion )
                    || '  ', '/') WITHIN GROUP(
                ORDER BY
                    ( p.aud_fecha_adicion ))
        FROM
            mat.mat_matricula p,
            org.org_periodo_estructura pex
        WHERE
                p.ins_codigo = c.ins_codigo
            AND p.mat_tipo_proceso = 'R'
            AND p.aud_eliminado = 'N'
            AND p.mat_anulado = 'N'
            AND  pex.pee_codigo  = p.pee_codigo
            and pex.pel_codigo=o.pel_codigo
    )                                                    fecha_pre_matricula,

    ( decode(c.ins_aprobado, 'S', 'SI', 'NO') )            AS "CONFIRMADO POR ADMISIONES?",


    /*DECODE(NVL(( select g.fis_estado --decode(g.fis_estado,'C','SI','I','NO')
    from SBE.sbe_ficha_socioeconomica g
    where g.alu_codigo= c.alu_codigo
    ),'I'),'C','SI','NO')                                            AS "REGISTRO FICHA SOCIOECONOMICA?",*/


    ( decode(c.ins_anticipo, 'S', 'SI', 'NO') )            AS "CONFIRM? ANTICIPO MATRICULA?",

    ( decode((
        SELECT DISTINCT
            paf.paf_pagado_anticipo
        FROM
            fac.fac_pago_factura paf
        WHERE
                paf.ins_codigo = c.ins_codigo
            AND paf.tip_codigo = 1
            AND ROWNUM = 1
    ),
             'S',
             'SI',
             'NO') )                                            AS "PAGO ANTICIPO MATRICULA?",


    ( nvl((
        SELECT DISTINCT
            r.rean_valor
        FROM
            fac.fac_pago_factura             f, fac.fac_encabezado_prefactura    p,
            ins.ins_inscripcion              i,
            easi.registro_anticipos          r
        WHERE
                i.ins_codigo = f.ins_codigo
            AND f.tip_codigo = 1
            AND f.enp_codigo = p.enp_codigo
            AND r.enve_numero = p.enve_numero
            AND i.ins_codigo = c.ins_codigo
            AND ROWNUM = 1
    ),
          0) )                                                  AS "VALOR PAGADO POR ANTICIPO MATRICULA",

    ( decode((
        SELECT DISTINCT
            a1.als_autorizacion
        FROM
            sna.sna_alumno_senescyt a1
        WHERE
                upper(a1.als_nro_identificacion) = upper(cl.cllc_ruc)
            AND a1.car_codigo = pre.car_codigo_leg
            AND a1.fac_codigo = fac_codigo_leg
            AND a1.sed_codigo = estcampus.est_codigo_padre
            AND a1.mod_codigo = mpe.mod_codigo
            AND a1.als_vigente = 'S'
            AND a1.als_eliminado = 'N'
    ),
             'S',
             'SI',
             'NO') )                                            AS "REGISTRA BECA SENESCYT?",


    ( decode((
        SELECT
            COUNT(*)
        FROM
            mat.mat_matricula       mat, fac.fac_pago_factura    paf
        WHERE
                mat.cllc_cdg = cl.cllc_cdg
            AND mat.mat_codigo = paf.ins_codigo
            AND paf.tip_codigo = 4
            AND paf.paf_pagado = 'S'
            AND mat.ins_codigo IS NOT NULL
    ),
             0,
             'NO',
             'SI') )                                            AS "ESTUDIANTE ANTIGUO DE UPS?",


    (
        SELECT
            COUNT(*)
        FROM
                 ins.ins_requisito_estructura es
            INNER JOIN ins.ins_requisito      q ON q.req_codigo = es.req_codigo
            INNER JOIN ins.ins_req_est_ins    req ON req.ree_codigo = es.ree_codigo
        WHERE
            req.ins_codigo = c.ins_codigo
        AND req.rei_entregado='S'
        --AND es.est_codigo = 3-- OJO
    )                                                    total_doc_entregados,

    decode((
        SELECT
            to_char(COUNT(*))
        FROM
                 ins.ins_requisito_estructura es
            INNER JOIN ins.ins_requisito      q ON q.req_codigo = es.req_codigo
            INNER JOIN ins.ins_req_est_ins    req ON req.ree_codigo = es.ree_codigo
        WHERE
                req.ins_codigo = c.ins_codigo
            AND q.req_codigo = 19
            AND req.rei_entregado='S'
            --AND es.est_codigo = 3
    ),
           '1',
           'SI',
           'NO')                                         "ENTREGO COPIA CERTIFICADA DEL TITULO DE BACHILLER?",

    decode((
        SELECT
            to_char(COUNT(*))
        FROM
                 ins.ins_requisito_estructura es
            INNER JOIN ins.ins_requisito      q ON q.req_codigo = es.req_codigo
            INNER JOIN ins.ins_req_est_ins    req ON req.ree_codigo = es.ree_codigo
        WHERE
                req.ins_codigo = c.ins_codigo
            AND q.req_codigo = 21
             AND req.rei_entregado='S'
             --AND es.est_codigo = 3
    ),
           '1',
           'SI',
           'NO')                                         "ENTREGO CERTIFICADO PROVISIONAL DE ESTUDIOS ?",

    decode((
        SELECT
            to_char(COUNT(*))
        FROM
                 ins.ins_requisito_estructura es
            INNER JOIN ins.ins_requisito      q ON q.req_codigo = es.req_codigo
            INNER JOIN ins.ins_req_est_ins    req ON req.ree_codigo = es.ree_codigo
        WHERE
                req.ins_codigo = c.ins_codigo
            AND q.req_codigo = 25
             AND req.rei_entregado='S'
             --AND es.est_codigo = 3
    ),
           '1',
           'SI',
           'NO')                                         "ENTREGA UNA FOTO A COLOR TAMA?O CARNET?",


    decode((
        SELECT
            to_char(COUNT(*))
        FROM
                 ins.ins_requisito_estructura es
            INNER JOIN ins.ins_requisito      q ON q.req_codigo = es.req_codigo
            INNER JOIN ins.ins_req_est_ins    req ON req.ree_codigo = es.ree_codigo
        WHERE
                req.ins_codigo = c.ins_codigo
            AND q.req_codigo = 26
             AND req.rei_entregado='S'
             --AND es.est_codigo = 3
    ),
           '1',
           'SI',
           'NO')                                         "ENTREGA CEDULA?",

        decode((
        SELECT
            to_char(COUNT(*))
        FROM
                 ins.ins_requisito_estructura es
            INNER JOIN ins.ins_requisito      q ON q.req_codigo = es.req_codigo
            INNER JOIN ins.ins_req_est_ins    req ON req.ree_codigo = es.ree_codigo
        WHERE
                req.ins_codigo = c.ins_codigo
            AND q.req_codigo = 27
             AND req.rei_entregado='S'
             --AND es.est_codigo = 3
    ),
           '1',
           'SI',
           'NO')                                         "ENTREGO COPIA A COLOR DEL CERTIFICADO DE VOTACION ACTUALIZADO (PARA MAYORES DE EDAD)?",

    decode((
        SELECT
            to_char(COUNT(*))
        FROM
                 ins.ins_requisito_estructura es
            INNER JOIN ins.ins_requisito      q ON q.req_codigo = es.req_codigo
            INNER JOIN ins.ins_req_est_ins    req ON req.ree_codigo = es.ree_codigo
        WHERE
                req.ins_codigo = c.ins_codigo
            AND q.req_codigo = 28
             AND req.rei_entregado='S'
             --AND es.est_codigo = 3
    ),
           '1',
           'SI',
           'NO')                                         "ENTREGO COPIA CERTIFICADA DEL ACTA DE GRADO?",

    decode((
        SELECT
            to_char(COUNT(*))
        FROM
                 ins.ins_requisito_estructura es
            INNER JOIN ins.ins_requisito      q ON q.req_codigo = es.req_codigo
            INNER JOIN ins.ins_req_est_ins    req ON req.ree_codigo = es.ree_codigo
        WHERE
                req.ins_codigo = c.ins_codigo
            AND q.req_codigo = 34
             AND req.rei_entregado='S'
             --AND es.est_codigo = 3
    ),
           '1',
           'SI',
           'NO')                                         "ENTREGO CERTIFICADO DIGITAL DEL REGISTRO DE TITULO DE BACHILLER?",

    (
        SELECT
            LISTAGG('  '
                    || arg_nombre
                    || '  ', '/') WITHIN GROUP(
                ORDER BY
                    arg_nombre
            )
        FROM
            org.org_area_geografica
        WHERE
            tag_codigo = 1
        CONNECT BY
            arg_codigo = PRIOR arg_codigo_padre
        START WITH arg_codigo = a.arg_codigo_dom
    )                                                    pais_domicilio,

    (
        SELECT
            LISTAGG('  '
                    || arg_nombre
                    || '  ', '/') WITHIN GROUP(
                ORDER BY
                    arg_nombre
            )
        FROM
            org.org_area_geografica
        WHERE
            tag_codigo = 3
        CONNECT BY
            arg_codigo = PRIOR arg_codigo_padre
        START WITH arg_codigo = a.arg_codigo_dom
    )                                                    provincia_domicilio,

    (
        SELECT
            LISTAGG('  '
                    || arg_nombre
                    || '  ', '/') WITHIN GROUP(
                ORDER BY
                    arg_nombre
            )
        FROM
            org.org_area_geografica
        WHERE
            tag_codigo = 4
        CONNECT BY
            arg_codigo = PRIOR arg_codigo_padre
        START WITH arg_codigo = a.arg_codigo_dom
    )                                                    ciudad_domicilio,

    o.pel_codigo
FROM
    sigac.cliente_local                   cl,
    ins.ins_alumno                        a,
    ins.ins_inscripcion                   c,
    ins.ins_oferta_inscripcion_inicial    o,
    ins.ins_ins_pro_aca                   t,
    ped.ped_modalidad                  MOD,
    ped.ped_mod_pro_edu                   mpe,
    ped.ped_proyecto_educacion            pre,
    ped.ped_tipo_proyecto_educacion       tpe,
    ped.ped_est_mod_pro_edu               emp,
    ped.ped_des_est_pro_edu               dep,
    org.org_descripcion_estructura        dee,
    org.org_estructura                    est,
    org.org_estructura                    estcampus,
    org.org_descripcion_estructura        deecampus
WHERE
        cl.cllc_cdg = a.cllc_cdg
    AND a.alu_codigo = c.alu_codigo
    AND c.oii_codigo = o.oii_codigo
    AND c.ins_codigo = t.ins_codigo
    AND mpe.mpe_codigo = t.mpe_codigo
    AND tpe.tpe_codigo = pre.tpe_codigo
    AND pre.pre_numero = mpe.pre_numero
    AND emp.mpe_codigo = mpe.mpe_codigo
    AND    mpe.mod_codigo        = MOD.mod_codigo
    AND dep.pre_numero = pre.pre_numero
    AND dep.dee_codigo = dee.dee_codigo
    AND dee.dee_codigo = est.dee_codigo
    AND est.est_codigo = o.est_codigo
    AND est.est_codigo_padre = emp.est_codigo
    AND estcampus.est_codigo = emp.est_codigo
    AND deecampus.dee_codigo = estcampus.dee_codigo
    AND t.ipa_vigente = 'S'
    AND o.pel_codigo >=61
    AND o.pel_codigo <=70
    
    """,
    "select * from bsc.ver_inscripciones_act where pel_codigo>60 and sede='GUAYAQUIL'",
    "select * from bsc.ver_inscripciones_act where pel_codigo>60",
    """
    SELECT pel.pel_descripcion||'('||pel.pel_codigo||')' periodo,
       dsed.dee_descripcion sede,
       dcam.dee_descripcion campus,
       PER.per_nro_identificacion   nro_identificacion,
       PER.per_apellidos||' '||PER.per_nombres docente,
       dcar.dee_descripcion carrera_estudiante,
       tpe.tpe_descripcion                                      tipo_proyecto,
       cli.cllc_ruc cedula,
       alu.alu_apellidos apellidos,
       alu.alu_nombres nombres,
       MAT.MAT_NIVEL nivel,
       jor.jor_descripcion,
       DECODE(mal.mal_tipo,'C','MATERIA COM N','G','MATERIA GEN RICA','ESPECIFICA - ' || +dcar.dee_descripcion) carrera_tipo,
       mte.mat_descripcion,
       gru.gru_item grupo,
       cal.cal_nota_final nota_final,
       CAL.CAL_NUMERO_VECES,
       cal.aud_fecha_modificacion fecha_paso_calificaciones,
       decode(cal.cal_aprobado,'S','APROBADO','N','REPROBADO') aprobado,
       decode(paf.paf_pagado,'S','SI','N','NO')  pagado,
       alu.alu_email                             EMAIL_INS,
       cli.cllc_email                                         EMAIL_PERS,
       nvl2 (cli.cllc_celular, ''''||cli.cllc_celular, '')    CELULAR,
       nvl2 (cli.cllc_fono, ''''||cli.cllc_fono, '')          TELEFONO,
      cli.cllc_calle||' '||DECODE(cli.cllc_nmr,NULL,'S/N',cli.cllc_nmr) DIRECCION





FROM   mat.mat_matricula                        mat,
       cal.cal_calificacion                     cal,
       ped.ped_malla                            mal,
       fac.fac_pago_factura                     paf,
       org.org_periodo_estructura               pee,
       org.org_periodo_lectivo                  pel,
       ped.ped_est_mod_pro_edu                  emp,
       org.org_estructura                       cam,
       org.org_descripcion_estructura           dcam,
       org.org_estructura                       sed,
       org.org_descripcion_estructura           dsed,
       org.org_descripcion_estructura           dcar,
       ins.ins_alumno                           alu,
       sigac.cliente_local                      cli,
       ofe.ofe_grupo                            gru,
       ped.ped_des_est_pro_edu                  dep,
       ofe.ofe_distributivo                     dis,
       gth.gth_persona                          PER,
       ped.ped_materia                          mte,
       ped.ped_proyecto_educacion               pre,
       PED.ped_tipo_proyecto_educacion          tpe,
       ins.ins_inscripcion                      inc,
       ins.ins_oferta_inscripcion_inicial       oii,
       ins.ins_jornada                          jor


WHERE  mat.mat_codigo = cal.mat_codigo
AND    mat.mat_codigo = paf.ins_codigo
AND    paf.tip_codigo = 4
AND    cal.mal_codigo = mal.mal_codigo
AND    mal.mat_codigo = mte.mat_codigo
--AND    mal.mal_codigo_oferta = 'G-HU-002'
AND    mat.pee_codigo = pee.pee_codigo
AND    mat.dep_codigo = dep.dep_codigo
AND    dep.dee_codigo = dcar.dee_codigo
AND    mat.emp_codigo = emp.emp_codigo
AND    emp.est_codigo = cam.est_codigo
AND    cam.dee_codigo = dcam.dee_codigo
AND    cam.est_codigo_padre = sed.est_codigo
AND    sed.dee_codigo = dsed.dee_codigo
AND    mat.cllc_cdg = cli.cllc_cdg
AND    cli.cllc_cdg = alu.cllc_cdg
AND    cal.gru_codigo = gru.gru_codigo
AND    pee.pel_codigo = pel.pel_codigo
and  pee.pel_codigo BETWEEN 62 AND 70
--AND    sed.est_codigo IN (3)
AND    cal.aud_eliminado = 'N'
AND    cal.cal_retirado = 'N'
--and    mat.mat_pagado = 'S'
AND    gru.gru_abierto = 'S'
AND    dis.gru_codigo  = gru.gru_codigo
AND    dis.per_codigo  = PER.per_codigo
AND    dis.dis_activo  = 'S'
AND    dis.dis_tipo_rol = 'T'

AND    dep.pre_numero = pre.pre_numero
AND    pre.tpe_codigo                                         IN (4,5,12,14) --Todos -- 4 redise ados -- 5unificados - 12 Ajuste
AND    PRE.tpe_codigo                                         = tpe.tpe_codigo
AND    tpe.ctp_codigo             = 2
--and    CAL.CAL_NOTA_FINAL = 0
--AND    per.per_nro_identificacion ='0910815430'
--AND      dcar.dee_descripcion LIKE '%TELECO%'

--AND      PER.per_apellidos LIKE '%YELA%'
--AND      PER.per_apellidos LIKE '%LIBERIO%'
--AND      PER.per_apellidos LIKE '%VILLACIS%'
--AND      PER.per_apellidos LIKE '%BOSQUEZ%' AND PER.per_nombres LIKE '%ANGEL%'
--AND      PER.per_apellidos LIKE '%MENDEZ%'
--AND      PER.per_apellidos LIKE '%PELAEZ%'
--AND      PER.per_apellidos LIKE '%RODRIGUEZ%' AND PER.per_nombres LIKE '%MARTHA%'
--AND      PER.Per_Apellidos LIKE '%SANMARTIN%'
--AND      PER.Per_Apellidos LIKE '%TAMAYO%'

and      mat.ins_codigo       = inc.ins_codigo
AND      inc.oii_codigo       = oii.oii_codigo
AND      oii.jor_codigo       = jor.jor_codigo
--0603274226
ORDER  BY  grupo,apellidos,nombres desc nulls last
    """,
    """
    SELECT DISTINCT

       pel.pel_descripcion                            periodo,  
       eep.est_codigo,
      (SELECT dsed.dee_descripcion
       FROM   org.org_estructura sed,
              org.org_descripcion_estructura dsed
       WHERE  dsed.dee_codigo = sed.dee_codigo
       AND    sed.est_nivel = 2
       CONNECT BY PRIOR sed.est_codigo_padre = sed.est_codigo
       START  WITH sed.est_codigo = dis.est_codigo)   sede,
      (SELECT dsed.dee_descripcion
       FROM   org.org_estructura sed,
              org.org_descripcion_estructura dsed
       WHERE  dsed.dee_codigo = sed.dee_codigo
       AND    sed.est_nivel = 3
       CONNECT BY PRIOR sed.est_codigo_padre = sed.est_codigo
       START  WITH sed.est_codigo = dis.est_codigo)   campus,
      (SELECT NVL(dsed.dee_descripcion,mal.mal_tipo)
       FROM   org.org_estructura sed,
              org.org_descripcion_estructura dsed
       WHERE  dsed.dee_codigo = sed.dee_codigo
       AND    sed.est_nivel = 4
       CONNECT BY PRIOR sed.est_codigo_padre = sed.est_codigo
       START  WITH sed.est_codigo = dis.est_codigo)   carrera_paracademico,
       --pre.tpe_codigo TIPO_MALLA,
       decode(mal.mal_tipo, 'E', decode(pre.tpe_codigo, 12, 'AJUSTE CURRICULAR', 5, 'UNIFICADO',4, 'NUEVA O REDISEÑO'))  Tipo_malla,
       act.act_descripcion                            actividad,
       mat.mat_descripcion                            materia,
       decode(mal.mal_tipo, 'E', 'ESPECIFICA',NULL,'VARIOS O PRACTICAS', 'COMUN Y GENERICA')TIPO_MATERIA, 
       DECODE(gru.mod_codigo,1,'PRESENCIAL',2,'SEMIPRESENCIAL',3,'DISTANCIA',4,'EN LINEA',5,'DUAL',6,'HÍBRIDA') MODALIDAD,
        decode(gru.gru_tipo_componente,'D','DOCENCIA', 'P','PRACTICA') tipo_componente,
       decode(gru.gru_planificado,'N','NO','S','SI',NULL) silabo_planificado,
       mal.mal_nivel NIVEL_MATERIA,
       --mal.pre_numero,
       gru.gru_descripcion                            grupo,
       gru.gru_cupos CUPOS_PROYECTADOS,
       gru.gru_cupos_utilizados CUPOS_UTILIZADOS,
       hor.hor_dia,
       decode(hor.hor_dia, 1,'LUNES', 
                           2,'MARTES', 
                           3,'MIERCOLES',
                           4,'JUEVES',
                           5,'VIERNES',
                           6,'SABADO',
                           7,'DOMINGO')               dia,
       to_char(hor.hor_hora_inicio, 'HH24:MI')        hora_inicio,
       to_char(hor.hor_hora_fin, 'HH24:MI')           hora_fin,
       dis.dis_fecha_inicio                           desde,
       dis.dis_fecha_fin                              hasta,
       blo.blo_descripcion                            bloque,
        esf.esf_codigo ,
       esf.esf_descripcion                            lugar,
       esf.esf_capacidad                              capacidad,
       ''''||per.per_nro_identificacion            	      n_indentificacion,
       per.per_apellidos||' ' ||per.per_nombres       docente


FROM   ofe.ofe_esp_fis_est_per eep,
       ofe.ofe_horario         hor,
       ofe.ofe_distributivo    dis,
       ofe.ofe_grupo           gru,
       org.org_espacio_fisico  esf,
       org.org_bloque          blo,
       org.org_periodo_lectivo pel,
       org.org_periodo_estructura pee,
       gth.gth_persona         per,
       ped.ped_malla           mal,
       ped.ped_materia         mat,
       ofe.ofe_actividad       act,
       ped.ped_proyecto_educacion        pre

WHERE  eep.esf_codigo = esf.esf_codigo
AND    esf.blo_codigo = blo.blo_codigo
AND    hor.eep_codigo = eep.eep_codigo
AND    dis.dis_codigo = hor.dis_codigo
AND    dis.per_codigo = per.per_codigo(+)
AND    dis.gru_codigo = gru.gru_codigo(+)
AND    gru.mal_codigo = mal.mal_codigo(+)
AND    mal.mat_codigo = mat.mat_codigo(+)
AND    dis.act_codigo = act.act_codigo(+)
AND    eep.pel_codigo = pel.pel_codigo
AND    pel.pel_codigo = pee.pel_codigo
AND    pee.est_codigo IN (SELECT  est.est_codigo
                          FROM    org.org_estructura est
                          WHERE   est.est_codigo_padre = 3)
AND    dis.aud_eliminado    = 'N'
AND    gru.aud_eliminado(+) = 'N'
AND    hor.aud_eliminado    = 'N'
AND    eep.pel_codigo > (64)--:PV_PEL_CODIGO  
--OR    pee.pee_fecha_final BETWEEN dis.dis_fecha_inicio AND dis.dis_fecha_fin OR dis.dis_fecha_fin BETWEEN pee.pee_fecha_inicial AND pee.pee_fecha_final
AND    eep.est_codigo IN (SELECT  est.est_codigo
                          FROM    org.org_estructura est
                          CONNECT BY PRIOR est.est_codigo = est.est_codigo_padre
                          START   WITH est.est_codigo     = 3)

AND pre.pre_numero = mal.pre_numero
--AND pre.tpe_codigo IN (4,5,12,11)

ORDER  BY periodo,sede,campus,bloque,lugar,hor.hor_dia,hora_inicio,docente
    """,
    "select * from expl.certificaciones",
    """
    SELECT distinct ina.sed_codigo                                SED_CODIGO,
       sed.sed_descripcion                                    SEDE,
       ina.pel_codigo                                         PEL_CODIGO,
       pel.pel_descripcion                                    PERIODO_LECTIVO,
       cli.cllc_cdg                                           CODIGO,
       cli.cllc_ruc                                           CEDULA,
       trim(alu.alu_apellidos) APELLIDOS,
       trim(alu.alu_nombres)    NOMBRE,
        ina.car_codigo_proyecto ||' '||car.car_descripcion     CARRERA,
       alu.alu_email                                          CORREO_INSTITUCIONAL,
       cli.cllc_email                                         CORREO_PERSONAL,
       cli.cllc_fono                                          TELEFONO_1,
       cli.cllc_fono2                                         TELEFONO_2,
       cli.cllc_celular                                       CELULAR,
       decode(alu.alu_operadora_celular,'M','Movistar',
       decode(alu.alu_operadora_celular,'P','Claro',
       decode(alu.alu_operadora_celular,'A','Alegro',
       decode(cli.cllc_celular,null,null,'No registrado'))))  OPERADORA,
        1                                                     NUMERO,
        ina.ina_fecha_adicion,
                         (CASE
                             WHEN (select l.tipo_matricula from matgeneral l where l.est_sed_leg=1 and l.mat_tipo_proceso='R'and l.periodo=65 and l.identificacion=cli.cllc_ruc )='ORDINARIA' THEN 'MATRICULADO'
                             WHEN (select l.tipo_matricula from matgeneral l where l.est_sed_leg=1 and l.mat_tipo_proceso='R'and l.periodo=65 and l.identificacion=cli.cllc_ruc ) IS NULL THEN 'NO MATRICULADO'
                             END)ESTADO

FROM   sna.sna_periodo_lectivo pel,
       sna.sna_sede sed,
       sna.sna_campus cam,
       sna.sna_inscripcion_academico ina,
       sna.sna_alumno alu,
       sigac.cliente_local cli,
       sna.sna_carrera car
WHERE  ina.pel_codigo                   = pel.pel_codigo
AND    ina.sed_codigo                   = sed.sed_codigo
AND    ina.cam_codigo_proyecto          = cam.cam_codigo
AND    ina.sed_codigo                   = cam.sed_codigo
AND    ina.cllc_cdg                     = alu.cllc_cdg
AND    ina.cllc_cdg                     = cli.cllc_cdg
AND    alu.cllc_cdg                     = ina.cllc_cdg
AND    ina.ina_anulado                  = 'N'
AND    ina.ina_eliminado                = 'N'
AND    ina.ina_vigencia                 = 'S'
AND    ina.ina_pagado                   = 'S'
AND    ina.peL_codigo                   >= 65 --:pv_pel_codigo
AND    TO_CHAR(ina.sed_codigo)         = 1 --LIKE  :pv_sed_codigo
AND    ina.car_codigo_proyecto          = car.car_codigo
AND    ina.fac_codigo_proyecto          = car.fac_codigo
AND     ina.car_codigo_proyecto=36
ORDER BY ina.sed_codigo,
         ina.pel_codigo,
         trim(alu.alu_apellidos),
         trim(alu.alu_nombres)
"""
  ]
DIRECTORIES = [
    '/root/OneDrive/REPORTERIA/Matricula',
    '/root/OneDrive/REPORTERIA/Listado Estudiantes Matriculados',
    '/root/OneDrive/REPORTERIA/Listado Estudiantes Prematriculados',
    '/root/OneDrive/REPORTERIA/Listado Docentes Materias',
    '/root/OneDrive/REPORTERIA/Listado Aspirantes Confirmados',
    '/root/OneDrive/REPORTERIA/Listado Aspirantes/GUAYAQUIL',
    '/root/OneDrive/REPORTERIA/Listado Aspirantes/NACIONAL',
    '/root/OneDrive/REPORTERIA/Notas Finales',
    '/root/OneDrive/REPORTERIA/Espacio Fisico',
    '/root/OneDrive/REPORTERIA/Certificaciones',
    '/root/OneDrive/REPORTERIA/Datos_Personales_Aspirantes',
]



