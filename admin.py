import os
import glob
import cx_Oracle
import xlsxwriter
from datetime import datetime
import shutil
from configuraciones import (ORA_CONNECTION, ORA_USERNAME, ORA_PASSWORD, SQLS, DIRECTORIES)

def get_oracle(connection, username, password):
    try:
        conn = cx_Oracle.connect(username, password, connection)
        return conn
    except cx_Oracle.Error as e:
        print(f"Error al conectar a la base de datos: {e}")
        return None

def write_excel(filename, fields, contents):
    with xlsxwriter.Workbook(filename) as workbook:
        format_title = workbook.add_format({'bold': True, 'align': 'center', 'valign': 'vcenter'})
        format_content = workbook.add_format({'align': 'center', 'valign': 'vcenter'})
        sheet = workbook.add_worksheet('sheet1')
        sheet.freeze_panes(1, 0)
        for field in range(len(fields)):
            sheet.write(0, field, fields[field][0], format_title)
        for row in range(len(contents)):
            for col in range(len(fields)):
                ceil = contents[row][col]
                if ceil is not None:
                    sheet.write(row + 1, col, str(ceil), format_content)
    print(f"Guardado en {filename}")

def execute_and_export(sql, directory, specific_folder):
    print('-----------------start------------------')
    print(f"Iniciando ejecución de la consulta para la carpeta: {specific_folder} en el directorio: {directory}")
    now = datetime.now() 
    time = now.strftime("%H%M%S")
    date = now.strftime("%m%d%Y")
    m_file = f'{specific_folder}_detalle_{date}_{time}.xlsx'

    # Verificar si el directorio existe, si no, crearlo
    full_directory_path = os.path.join(directory, specific_folder)
    os.makedirs(full_directory_path, exist_ok=True)

    # Limpiar archivos existentes en la carpeta asignada
    files = glob.glob(f'{full_directory_path}/*')
    for f in files:
        os.remove(f)


    conn = get_oracle(ORA_CONNECTION, ORA_USERNAME, ORA_PASSWORD)
    if conn is None:
        print(f"Falló la conexión a la base de datos para la carpeta: {specific_folder}")
        return
    try:
        with conn.cursor() as cursor:
            print(f"Ejecutando SQL: {sql}")
            result = cursor.execute(sql)
            xlsx_fields = cursor.description
            xlsx_contents = cursor.fetchall()
            print(f"Consulta completada. Filas obtenidas: {len(xlsx_contents)}")
            output_file_path = os.path.join(full_directory_path, m_file)
            write_excel(output_file_path, xlsx_fields, xlsx_contents)

            print(f"Guardado en {output_file_path}")
            print(f'Tiene {str(len(xlsx_contents))} filas, {str(len(xlsx_fields))} columnas')
    except cx_Oracle.DatabaseError as e:
        print(f"Error al ejecutar la consulta: {e}")
    finally:
        conn.close()
    print(f'Finalizada la ejecución para la carpeta: {specific_folder} en el directorio: {directory}')
    print('-------------------end------------------')


if __name__ == '__main__':
    for sql, dir_and_folder in zip(SQLS, DIRECTORIES):
        directory, specific_folder = dir_and_folder.rsplit('/', 1)
        print(specific_folder)
        execute_and_export(sql, directory, specific_folder)
