# Utilizar una imagen base de Python
FROM python:3.9-slim

# Instalar dependencias necesarias
RUN apt-get update && \
    apt-get install -y libaio1 wget unzip && \
    apt-get clean

# Instalar dependencias necesarias, incluyendo onedrive CLI
RUN apt-get update && apt-get install -y \
    curl fuse gcc make \
    && apt-get install -y libcurl4-openssl-dev \
    && apt-get install -y onedrive \
    && apt-get clean

# Descargar e instalar Oracle Instant Client
RUN wget https://download.oracle.com/otn_software/linux/instantclient/211000/instantclient-basiclite-linux.x64-21.1.0.0.0.zip && \
    unzip instantclient-basiclite-linux.x64-21.1.0.0.0.zip && \
    mv instantclient_21_1 /usr/lib/oracle && \
    if [ ! -f /usr/lib/oracle/libclntsh.so ]; then ln -s /usr/lib/oracle/libclntsh.so.21.1 /usr/lib/oracle/libclntsh.so; fi && \
    rm instantclient-basiclite-linux.x64-21.1.0.0.0.zip

# Configurar la variable de entorno ORACLE_HOME
ENV LD_LIBRARY_PATH=/usr/lib/oracle

# Crear un directorio de trabajo
WORKDIR /app

# Copiar archivos|
COPY requirements.txt .
COPY admin.py .
COPY configuraciones.py .
# **Copiar los archivos de configuración de OneDrive al directorio correcto**
COPY items.sqlite3 /root/.config/onedrive/
COPY refresh_token /root/.config/onedrive/
# Instalar dependencias de Python
RUN pip install --no-cache-dir -r requirements.txt
# Comando por defecto
CMD ["python", "/app/admin.py"]
