import os

# Lista de directorios a limpiar
DIRECTORIES = [
    '/root/OneDrive/REPORTERIA/Matricula',
    '/root/OneDrive/REPORTERIA/Listado Estudiantes Matriculados',
    '/root/OneDrive/REPORTERIA/Listado Estudiantes Prematriculados',
    '/root/OneDrive/REPORTERIA/Listado Docentes Materias',
    '/root/OneDrive/REPORTERIA/Listado Aspirantes Confirmados',
    '/root/OneDrive/REPORTERIA/Listado Aspirantes/GUAYAQUIL',
    '/root/OneDrive/REPORTERIA/Listado Aspirantes/NACIONAL',
    '/root/OneDrive/REPORTERIA/Notas Finales',
    '/root/OneDrive/REPORTERIA/Espacio Fisico',
    '/root/OneDrive/REPORTERIA/Certificaciones',
    '/root/OneDrive/REPORTERIA/Datos_Personales_Aspirantes',
]

def delete_excel_files(directory):
    """Elimina archivos de Excel (.xlsx y .xls) en el directorio especificado."""
    for root, dirs, files in os.walk(directory):
        for file in files:
            if file.endswith('.xlsx') or file.endswith('.xls'):
                file_path = os.path.join(root, file)
                print(f'Eliminando archivo de Excel: {file_path}')
                os.remove(file_path)


# Limpiar archivos de Excel en todos los directorios de la lis
for directory in DIRECTORIES:
    if os.path.exists(directory):
        print(f'Limpiando archivos de Excel en el directorio: {directory}')
        delete_excel_files(directory)
    else:
        print(f'El directorio {directory} no existe.')
